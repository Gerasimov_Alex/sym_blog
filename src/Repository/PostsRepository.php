<?php

namespace App\Repository;

use App\Entity\Posts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Posts|null find($id, $lockMode = null, $lockVersion = null)
 * @method Posts|null findOneBy(array $criteria, array $orderBy = null)
 * @method Posts[]    findAll()
 * @method Posts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Posts::class);
    }


	/**
	 * Возвращает указанное количество случайно выбранных записей
	 *
	 * @param $number
	 *
	 * @return Posts[]|null
	 */
    public function getRandomPosts($number)
	{
		$postsId = $this->createQueryBuilder('p')
			->orderBy('p.id', 'ASC')
			->select('p.id')
			->getQuery()
			->getResult()
			;

		$maxNumber = count($postsId) - 1;
		if ($maxNumber <= 0) {
			return null;
		}

		$number = $tempNumber = $maxNumber > $number ? $number : $maxNumber;
		$idList = [];
		while ($tempNumber) {
			$rowNum = rand(0, $maxNumber);
			$id = $postsId[ $rowNum ][ 'id' ];
			if (!in_array($id, $idList)) {
				$idList[] = $id;
				$tempNumber--;
			}
		}

		return $this->findBy(['id' => $idList], [], $number);
	}
}

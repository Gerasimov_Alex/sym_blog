<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminBlogController extends AbstractController
{
    /**
     * @Route("/admin/blog/{page<\d+>?1}", name="admin_blog")
     */
    public function index()
    {
        return $this->render('admin/blog.html.twig', [
            'controller_name' => 'AdminBlogController',
        ]);
    }
}

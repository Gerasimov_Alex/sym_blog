<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Form\BlogPostsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class BlogController extends AbstractController
{
	/**
	 * @param int $page
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @Route("/blog/{page<\d+>?1}", name="app_blog")
	 */
	public function list($page)
	{
		# ToDo: Разобраться, как сделать правильно сделать постраничную навигацию.
		$postPerPage = 5;
		$navOffset = $postPerPage * ( $page - 1 );

		$entityManager = $this->getDoctrine()->getManager();
		$postRepository = $entityManager->getRepository(Posts::class);
		$postsCount = $postRepository->count([]);

		# ToDo: Узнать, возможно ли ограничить длину получаемого текста. Ограничить выборку только нужными полями.
		$posts = $postRepository->findBy([], [], $postPerPage, $navOffset);
		if ($page > 1 && !$posts) {
			# ToDo: Разобраться с языковыми константами.
			throw $this->createNotFoundException("Страница #$page не найдена");
		}

		return $this->render(
			'blog/list.html.twig',
			[
				'header' => 'Список записей',
				'page'   => $page,
				'pages'  => ceil($postsCount / $postPerPage),
				'posts'  => $posts,
				'count'  => $postsCount,
			]
		);
	}


	/**
	 * @param int $id
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @Route("/blog/post/{id<\d+>}", name="app_blog_detail")
	 */
	public function detail($id)
	{
		$entityManager = $this->getDoctrine()->getManager();
		$post = $entityManager->getRepository(Posts::class)->findOneBy([ 'id' => $id ]);

		if (!$post) {
			throw $this->createNotFoundException("Запись #$id не найдена");
		}

		if ($post->getLimited()) {
			$this->denyAccessUnlessGranted('ROLE_ADMIN');
		}

		return $this->render('blog/detail.html.twig', [ 'header' => 'detail', 'post' => $post ]);
	}


	/**
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @throws \Exception
	 *
	 * @Route("/blog/add/", name="app_blog_add")
	 */
	public function add(Request $request)
	{
		$result = false;
		$post = new Posts();
		$form = $this->createForm(BlogPostsType::class, $post);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			# ToDo: Сделать привязку постов к пользователю.
			$post->setDatetime(new \DateTime());
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($post);
			$entityManager->flush();

			$result = true;
		}

		return $this->render(
			'blog/add.html.twig',
			[
				'header' => 'Добавить запись',
				'form' => $form->createView(),
				'result' => $result
			]
		);
	}
}

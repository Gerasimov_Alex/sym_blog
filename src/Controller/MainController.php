<?php

namespace App\Controller;

use App\Entity\Posts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
	/**
	 * @Route("/", name="main")
	 */
	public function index()
	{
		$entityManager = $this->getDoctrine()->getManager();
		$postRepository = $entityManager->getRepository(Posts::class);
		$posts = $postRepository->findBy(['favorite' => 1], [], 8);

		return $this->render('main/index.html.twig', [
			'controller_name' => 'MainController',
			'posts'           => $posts,
		]);
	}
}

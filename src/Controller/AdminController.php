<?php

namespace App\Controller;

use App\Entity\Posts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }


	/**
	 * @param int $page
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @Route("/admin/blog/{page<\d+>?1}", name="admin_blog")
	 */
	public function list($page)
	{
		$postPerPage = 5;
		$navOffset = $postPerPage * ( $page - 1 );

		$entityManager = $this->getDoctrine()->getManager();
		$postRepository = $entityManager->getRepository(Posts::class);
		$postsCount = $postRepository->count([]);

		$posts = $postRepository->findBy([], [], $postPerPage, $navOffset);
		if ($page > 1 && !$posts) {
			throw $this->createNotFoundException("Страница #$page не найдена");
		}

		return $this->render(
			'admin/blog.html.twig',
			[
				'header' => 'Список записей',
				'page'   => $page,
				'pages'  => ceil($postsCount / $postPerPage),
				'posts'  => $posts,
				'count'  => $postsCount,
			]
		);
	}


	/**
	 * @param Posts $post
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @Route("/admin/favorite_switsh/{post<\d+>?1}", name="admin_favorite_switch")
	 */
	public function favoriteSwitch(Posts $post)
	{
		if ($post->getFavorite()) {
			$post->setFavorite(false);
		} else {
			$post->setFavorite(true);
		}
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->persist($post);
		$entityManager->flush();

		# ToDo: Добавить переадресацию на ту же страницу
		return $this->redirectToRoute('admin_blog');
	}


	/**
	 * @param Posts $post
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @Route("/admin/limited_switsh/{post<\d+>?1}", name="admin_limited_switch")
	 */
	public function limitedSwitch(Posts $post)
	{
		if ($post->getLimited()) {
			$post->setLimited(false);
		} else {
			$post->setLimited(true);
		}
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->persist($post);
		$entityManager->flush();

		return $this->redirectToRoute('admin_blog');
	}


	/**
	 * @param Posts $post
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @Route("/admin/post_delete/{post<\d+>?1}", name="admin_post_delete")
	 */
	public function postDelete(Posts $post)
	{
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->remove($post);
		$entityManager->flush();

		return $this->redirectToRoute('admin_blog');
	}
}

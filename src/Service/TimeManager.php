<?php


namespace App\Service;


class TimeManager
{
	/**
	 * Возвращает текущий день
	 *
	 * @return string
	 */
	public function getCurrentDay()
	{
		return date('w', time());
	}
}
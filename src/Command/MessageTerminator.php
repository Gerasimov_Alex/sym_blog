<?php


namespace App\Command;


use App\Entity\Posts;
use App\Service\TimeManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class MessageTerminator extends Command
{
	/**
	 * @var string
	 */
	protected static $defaultName = 'app:delete-messages';

	/**
	 * @var TimeManager
	 */
	protected $timeManager;

	/**
	 * @var EntityManagerInterface
	 */
	protected $em;

	public function __construct(TimeManager $manager, EntityManagerInterface $em)
	{
		$this->timeManager = $manager;
		$this->em = $em;

		parent::__construct();
	}


	protected function configure()
	{
		$this->setDescription('Удаление сообщений.')
			->setHelp('При запуске во сторник команда удалит одно случайно выборанное сообщение, в четверг -  два случайных сообщения.')
		;
	}


	public function execute(InputInterface $input, OutputInterface $output)
	{
		switch ($this->timeManager->getCurrentDay()) {
			case 2:
				static::delMessages(1);
				$output->writeln('Сегодня вторник. Удалено 1 сообщение.');
				break;
			case 4:
				static::delMessages(2);
				$output->writeln('Сегодня четверг. Удалено 2 сообщения.');
				break;
			default:
				$output->writeln('Сегодня не вторник и не четверг, ничего удалять не нужно.');
				break;
		}
	}


	/**
	 * Удаляет указанное число сообщений
	 *
	 * @param Integer $number
	 */
	protected function delMessages($number)
	{
		$posts = $this->em->getRepository(Posts::class)->getRandomPosts($number);
		foreach ($posts as $post) {
			$this->em->remove($post);
		}
		$this->em->flush();
	}
}
